export maximal_error
export compute_u_squared
export compute_variation
export compute_evolution
export compute_lyapunov_values
export compute_voltage_and_current
export write_solution_to_file

export lambda

function char_fxt(f,Δx)
    # return (x,t) -> 0.5*(f(x-0.5*Δx,t)+f(x-0.5*Δx,t))
    return f
end

function char_fx(f,Δx)
    # return x -> 0.5*(f(x-0.5*Δx)+f(x-0.5*Δx))
    return f
end


function lambda(TE)
    return 1/sqrt(TE.C*TE.L)
end



function compute_voltage_and_current(sol,TE)

    solsize=size(sol)
    volcur=Array{Float64,3}(undef,2,solsize[2],solsize[3])

    coeff=sqrt(TE.L/TE.C)

    for j in 1:solsize[3]
        for n in 1:solsize[2]
            volcur[1,n,j]=coeff*(sol[1,n,j]-sol[2,n,j])
            volcur[2,n,j]=sol[1,n,j]+sol[2,n,j]
        end
    end

    return volcur

end





function compute_variation(solution,Δx)
    @assert typeof(solution)==Array{Float64,3}

    TV=zeros(size(solution,3))

    for j in 1:size(solution,3)
        for i in 2:size(solution,2)
            for k in 1:2
                TV[j]+=abs(solution[k,i,j]-solution[k,i-1,j])
            end
        end
    end
    return Δx*TV
end

function compute_evolution(u)

    d=Vector{Float64}(undef,length(u)-1)
    for j in 1:length(u)-1
        d[j]=u[j+1]-u[j]
    end
    return d
end


function compute_lyapunov_values(v0,Tele,J,Δx)
    a=min(Tele.R/Tele.L,Tele.G/Tele.C)
    Δt=Tele.T/(J-1)

    V0=0
    for j in 1:size(v0,2)
        V0=V0+v0[1,j]^2+v0[2,j]^2
    end

    values=Vector{Float64}(undef,J)

    for j in 1:J
        values[j]=exp(-a*(j-1)*Δt)*V0*Δx
    end
    return values

end

function compute_u_squared(solution,Δx)
    @assert typeof(solution)==Array{Float64,3}

    u_squared=zeros(size(solution,3))

    for j in 1:size(solution,3)
        for i in 1:size(solution,2)
            for k in 1:2
                u_squared[j]+=solution[k,i,j]^2
            end
        end
    end
    return Δx*u_squared
end


function maximal_error(solution,exact)
    return maxy=maximum(abs.(solution-exact))
end


function write_solution_to_file(filename,time,sol,Δx)
    if(time==-1)
        time=size(sol,3)
    end

    X = Vector{Float64}(undef,size(sol,2)-2)
    for j in 2:size(sol,2)-1
        X[j-1] = (j-2)*Δx
    end
        X = ["X";X]
    
    xiplus = ["xiplus";sol[1,2:end-1,time]]
    ximinus = ["ximinus";sol[2,2:end-1,time]]
    open(filename, "w") do io
        DelimitedFiles.writedlm(io, [X xiplus ximinus], ',')
    end
end
