export plot_solution
export show_solution
export compare_solutions



function plot_solution(solution,Δt,TE,filename)

    #use at least 50 time steps and at most a hundredth of a second
    timestep=min(0.01,size(solution,3)*Δt/50)

    stride=1#Int(ceil(timestep/Δt))

    maxy=maximum(abs.(solution))

    anim=Plots.@animate for i in 1:stride:size(solution,3)
        plottitle=Printf.@sprintf("time = %.10f seconds",(i-1)*Δt)
        Plots.plot(range(0,stop=TE.l,length=size(solution,2)),[view(solution,1,:,i), view(solution,2,:,i)],title=plottitle,yaxis=("", (-maxy, maxy)),label=["Errors in ξ⁺(x)" "Errors in ξ⁻(x)"])
    end



    
    tempfilename=string(filename[1:end-4],"_raw.gif")
    # fullpath_tempfilename=string(ENV["HOME"],"/juliaplots/",tempfilename)
    fullpath_tempfilename=tempfilename

    frames=Int(round(1.0/Δt))

    Plots.gif(anim,fullpath_tempfilename,fps=frames)

    
    # fullpath_filename=string(ENV["HOME"],"/juliaplots/",filename)
    fullpath_filename=filename
    temp_mp4=string(fullpath_tempfilename[1:end-4],".mp4")
    end_mp4=string(fullpath_filename[1:end-4],".mp4")

    run(`ffmpeg -i $fullpath_tempfilename -r $frames -movflags faststart -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" $temp_mp4`)

    videotime=parse(Float64,chomp(read(`ffprobe -i $temp_mp4 -show_entries format=duration -v quiet -of csv="p=0"`,String)))
    conversion_factor=TE.T/videotime
    run(`ffmpeg -i $temp_mp4 -r $frames -filter:v "setpts=$conversion_factor *PTS" $end_mp4`,)
    
end

function show_solution(solution,Δt,TE)

    
    stride=Int(ceil(size(solution,3)/400.0))
    
    maxy=maximum(abs.(solution))
    
    for i in 1:stride:size(solution,3)
        # if((i-1)*Δt>0.5)
        #     break
        # end
        plot1=solution[1,:,i]
        plot2=solution[2,:,i]
        plottitle=Printf.@sprintf("time = %.10f seconds",(i-1)*Δt)
        Plots.plot(range(0,stop=TE.l,length=size(solution,2)),[plot1, plot2],title=plottitle ,yaxis=("", (-maxy, maxy))
                   ,label=["ξ⁺(x)" "ξ⁻(x)"],show=true)
        #readline()
    end

end


function compare_solutions(solution,exact_solution,Δt,TE)

    maxy=maximal_error(solution,exact_solution)
    Printf.@printf("maximal error = %.6e\n",maxy)
    stride=1
    for i in 1:stride:size(solution,3)
        # if((i-1)*Δt>0.5)
        #     break
        # end
        plottitle=Printf.@sprintf("time = %.10f seconds",(i-1)*Δt)
        Plots.plot(range(0,stop=TE.l,length=size(solution,2)),[solution[1,:,i]-exact_solution[1,:,i], solution[2,:,i]-exact_solution[2,:,i]],title=plottitle,yaxis=(" ", (-maxy, maxy)),label=["Errors in ξ⁺(x)" "Errors in ξ⁻(x)"],show=true)
        #readline()
    end
end
