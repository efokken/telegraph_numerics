export compute_steady_function
export compute_exact_solution



#This function computes the
function compute_steady_function(Tele,Telesteady)


    #assert well-defined parameters:
    @assert Tele.R>=0
    @assert Tele.L>0
    @assert Tele.G>=0
    @assert Tele.C>0
    @assert Tele.l>0
    @assert Tele.T>0
    @assert isreal(Telesteady.ω)
    @assert isreal(Telesteady.aV₀)
    @assert isreal(Telesteady.aVₗ)
    @assert isreal(Telesteady.ϕ₀)
    @assert isreal(Telesteady.ϕₗ)
    @assert Telesteady.aV₀>=0
    @assert Telesteady.aVₗ>=0
    # @assert Telesteady.ϕ₀>=0
    # @assert Telesteady.ϕₗ>=0


    #complex impedance:
    Z=Tele.R+im* Telesteady.ω *Tele.L

    
    #complex admittance
    Y=Tele.G+im* Telesteady.ω *Tele.C
    
    Y₀=sqrt(Y/Z)
    
    γ=sqrt(Y*Z)
    
    V₀=Telesteady.aV₀*exp(im*Telesteady.ϕ₀)
    Vₗ=Telesteady.aVₗ*exp(im*Telesteady.ϕₗ)
    #complex voltage:
    @inline function V(x)
        return (1/sinh(γ*Tele.l)*(Vₗ*sinh(γ*x)+V₀*sinh(γ*(Tele.l-x))))
    end

    #complex current:
    @inline function I(x)
        return (-Y₀/sinh(γ*Tele.l)*(Vₗ*cosh(γ*x)-V₀*cosh(γ*(Tele.l-x))))
    end

    #real voltage:
    function v(x,t)
        return real(V(x)*exp(im*Telesteady.ω*t))
    end

    #real current:
    function i(x,t)
        return real(I(x)*exp(im*Telesteady.ω*t))
    end
45
    function ξ⁺(x,t)
        return 0.5*(i(x,t)+sqrt(Tele.C/Tele.L)*v(x,t))
    end

    function ξ⁻(x,t)
        return 0.5*(i(x,t)-sqrt(Tele.C/Tele.L)*v(x,t))
    end

    return v,i,ξ⁺,ξ⁻
end

#creates array with exact solution
function compute_exact_solution(TE,TEs,N,J,Δt,Δx)
    
    v,i,ξ⁺,ξ⁻ = compute_steady_function(TE,TEs)

    char_ξ⁺=char_fxt(ξ⁺,Δx)
    char_ξ⁻=char_fxt(ξ⁻,Δx)

    exact_solution=Array{Float64,3}(undef,2,N,J)

    @inbounds for j in 1:J
        @inbounds for n in 1:N
            exact_solution[1,n,j]=char_ξ⁺((n-0.5)*Δx,(j-1)*Δt)
            exact_solution[2,n,j]=char_ξ⁻((n-0.5)*Δx,(j-1)*Δt)
        end
    end

    return exact_solution
end
