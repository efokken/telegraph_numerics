module Telegraphers

import Printf
import LinearAlgebra
import Plots
import StaticArrays
import DelimitedFiles

export initial_values

export compute_diagonal_exact_function
export compute_diagonal_exact_solution

export TEsteadydata
export TEdata
export example3

include("./Numerical_solver.jl")
include("./Steady_analytic_solver.jl")
include("./Plot_related.jl")
include("./Aux.jl")
include("./Network.jl")
include("../test/Network.jl")

#const R,L,G,C=0.028e-6,0.8e-6,15e-12,14e-12

struct TEsteadydata
    ω::Float64
    aV₀::Float64
    ϕ₀::Float64
    aVₗ::Float64
    ϕₗ::Float64
end

struct TEdata
    R::Float64
    L::Float64
    G::Float64
    C::Float64
    T::Float64
    l::Float64
end

function mydebugging()
    return false
end




function initial_values(x)
    if(x<1.5)
        return [x;-x]
    else
        return [3-x;-3+x]
    end

end


function compute_diagonal_exact_function(TE,init)

    @assert TE.R>=0
    @assert TE.L>0
    @assert TE.G>=0
    @assert TE.C>0

    @assert TE.R/TE.L == TE.G/TE.C "compute_diagonal_exact_function only works if the matrix B is diagonal"

    λ = 1/sqrt(TE.L*TE.C)
    b = 0.5*(TE.R/TE.L + TE.G/TE.C)

    P=StaticArrays.SMatrix{2,2}(0,1,1,0)

    function extended_init(x)
        refnumber=floor(x/TE.l)
        reference_point=TE.l*refnumber
        if(iseven(Int(refnumber)))
            return init(x-reference_point)
        else
            return P*init(TE.l-(x-reference_point))
        end

    end



    ξ⁺ = (x,t) -> exp(-b*t)*extended_init(x-λ*t)[1]
    ξ⁻ = (x,t) -> exp(-b*t)*extended_init(x+λ*t)[2]

    return ξ⁺,ξ⁻

end


function compute_diagonal_exact_solution(TE,N,J,Δt,Δx,init)

    ξ⁺,ξ⁻ = compute_diagonal_exact_function(TE,init)

    char_ξ⁺=char_fxt(ξ⁺,Δx)
    char_ξ⁻=char_fxt(ξ⁻,Δx)

    exact_solution=Array{Float64,3}(undef,2,N,J)

    @inbounds for j in 1:J
        @inbounds for n in 1:N
            exact_solution[1,n,j]=char_ξ⁺((n-1)*Δx,(j-1)*Δt)
            exact_solution[2,n,j]=char_ξ⁻((n-1)*Δx,(j-1)*Δt)
        end
    end

    return exact_solution

end


function example3(init_type)

    @assert(init_type=="exact"||init_type=="random")

    #angle velocity of the system
    ω=2pi/3
    l=3.0

    T=5.0
    Δx=l/200
    CFL_desired=0.9

    #values for the lines:
    TE=Vector{TEdata}(undef,3)
    TE[1]=TEdata(0.5,2.0,0.1,2.0,T,l)
    TE[2]=TEdata(0.5,3.0,0.1,4.0,T,2*l)
    TE[3]=TEdata(0.5,5.0,0.1,5.0,T,3*l)

    #number of spatial steps
    N = Vector{Int64}(undef,size(TE,1))
    for i in 1:size(TE,1)
        N[i]=Int(TE[i].l/Δx)
    end


    λ=Vector{Float64}(undef,size(TE,1))
    for i in 1:size(TE,1)
        λ[i]=1/sqrt(TE[i].L*TE[i].C)
    end
    Δt_candidate = CFL_desired*Δx/maximum(λ)
    J=Int(ceil(T/Δt_candidate)+1)
    Δt=T/(J-1)

    C=Δt/Δx*λ

    #set up ODE data:
    M=Vector{Array{Float64,2}}(undef,size(TE,1))
    for i in 1:size(TE,1)
        c=TE[i].R/TE[i].L+TE[i].G/TE[i].C
        b=TE[i].R/TE[i].L-TE[i].G/TE[i].C
        B=1/2*[c b;b c]
        M[i]=exp(-Δt/2*B)
    end



    #boundary conditions:
    V1=10.0+2.0im
    V2=50.0+2.0im
    V3=30.0+2.0im
    # V=[V1; V2; V3]
    V=[0;0;0]
    I_middle=100.0#-10.0im

    v_boundary=[(t -> real(V1*exp(im*ω*t))) (t -> real(V2*exp(im*ω*t))) (t -> real(V3*exp(im*ω*t)))]

    i_middle = (t -> real(I_middle*exp(im*ω*t)))


    #compute the correct voltage from the current condition in the middle

    Y=Vector{Complex{Float64}}(undef,size(TE,1))
    γ=Vector{Complex{Float64}}(undef,size(TE,1))
    a=Vector{Complex{Float64}}(undef,size(TE,1))
    b=Vector{Complex{Float64}}(undef,size(TE,1))

    for i in 1:size(TE,1)
        Z=TE[i].R+im*ω *TE[i].L
        W=TE[i].G+im*ω *TE[i].C
        Y[i]=sqrt(W/Z)
        γ[i]=sqrt(W*Z)
        a[i]=Y[i]/tanh(γ[i]*TE[i].l)
        b[i]=Y[i]/sinh(γ[i]*TE[i].l)
    end

    V_middle=(-I_middle+transpose(b)*V)/sum(a)

    TEs=Vector{TEsteadydata}(undef,size(TE,1))

    for i in 1:size(TE,1)
        TEs[i]=TEsteadydata(ω,abs(V[i]),angle(V[i]),abs(V_middle),angle(V_middle))
    end

    exact_solution=Vector{Array{Float64,3}}(undef,size(TE,1))
    for i in 1:size(TE,1)
        exact_solution[i]=compute_exact_solution(TE[i],TEs[i],N[i],J,Δt,Δx)
    end

    v_exact=Vector{Any}(undef,size(TE,1))
    i_exact=Vector{Any}(undef,size(TE,1))
    ξ⁺_exact=Vector{Any}(undef,size(TE,1))
    ξ⁻_exact=Vector{Any}(undef,size(TE,1))
    for i in 1:size(TE,1)
        v_exact[i],i_exact[i],ξ⁺_exact[i],ξ⁻_exact[i]=compute_steady_function(TE[i],TEs[i])
    end
    # v2_exact,i2_exact,ξ⁺2_exact,ξ⁻2_exact=compute_steady_function(TE[2],TEs[2])
    # v3_exact,i3_exact,ξ⁺3_exact,ξ⁻3_exact=compute_steady_function(TE[3],TEs[3])


    #The following code tests whether the node in the middle has the right current!

    i_test=Vector{Float64}(undef,J)
    for j in 1:J
        i_test[j]=i_exact[1](TE[1].l,(j-1)*Δt)+i_exact[2](TE[2].l,(j-1)*Δt)+i_exact[3](TE[3].l,(j-1)*Δt)-i_middle((j-1)*Δt)
    end


    #on to the numerical solution:

    solution=Vector{Array{Float64,3}}(undef,size(TE,1))
    for i in 1:size(TE,1)
        solution[i]=Array{Float64,3}(undef,2,N[i],J)
    end

    #auxiliary container for solution with ghost cells:
    ghost_and_values=Vector{Array{Float64,2}}(undef,size(TE,1))


    #for the first step only:

    #set initial condition:
    if(init_type=="exact")
        for i in 1:size(TE,1)
            for n in 1:N[i]
                solution[i][:,n,1]=[ξ⁺_exact[i]((n-0.5)*Δx,0);ξ⁻_exact[i]((n-0.5)*Δx,0)]
            end
        end
    else
        for i in 1:size(TE,1)
            solution[i][:,:,1]=rand(2,N[i])
        end
    end

    #fill it into our "working vector", containing the solution along with ghost cells.
    for i in 1:size(TE,1)
        ghost_and_values[i]= Array{Float64,2}(undef,2,N[i]+4)
        ghost_and_values[i][:,3:end-2]=view(solution[i],:,:,1)
    end

    #for progamming reasons we fill the current state into a new container:
    current_solution=Vector{Array{Float64,2}}(undef,size(TE,1))
    for i in 1:size(TE,1)
        current_solution[i]=view(solution[i],:,:,1)
    end

    #First time node conditions:
    #voltage nodes:

    #for derivative:
    voltage_ghost_values=Vector{Array{Float64,2}}(undef,3)
    for i in 1:3
        vnode_value=set_up_node_values([current_solution[i]],[1])
        voltage_ghost_values[i]=compute_ghost_cell_values(vnode_value,[TE[i]],[0],Δt,Δx,"voltage",v_boundary[i](0),v_boundary[i](-Δt))[1]
        ghost_and_values[i][:,1:2]=voltage_ghost_values[i]
    end

    #current_node:
        
    inode_value=set_up_node_values(current_solution,[-1;-1;-1])
    current_ghost_values=compute_ghost_cell_values(inode_value,TE,zeros(size(TE,1),1),Δt,Δx,"current",i_middle(0),i_middle(-Δt))

    for i in 1:size(TE,1)
        ghost_and_values[i][2:-1:1,end:-1:end-1]=current_ghost_values[i]
    end

    #first step:
    for i in 1:size(TE,1)
        solution[i][:,:,2]=compute_next_step(C[i],M[i],ghost_and_values[i],solution[i][:,:,2])
    end


    #following steps:

    for j in 2:J-1

        #fill the current values into the working vector:
        for i in 1:size(TE,1)
            ghost_and_values[i][:,3:end-2]=solution[i][:,:,j]
        end

        for i in 1:size(TE,1)
            current_solution[i]=view(solution[i],:,:,j)
        end

        #voltage nodes:
        for i in 1:3 #number of voltage nodes in this example:
            vnode_value=set_up_node_values([current_solution[i]],1)
            voltage_ghost_values[i]=compute_ghost_cell_values(vnode_value,[TE[i]],[voltage_ghost_values[i][2,2]],Δt,Δx,"voltage",v_boundary[i]((j-1)*Δt),v_boundary[i]((j-1)*Δt-Δt))[1]
            ghost_and_values[i]=fill_in_ghost_cells([ghost_and_values[i]],[1],[voltage_ghost_values[i]])[1]
        end

        #current_node:

        last_xi_0=Vector{Float64}(undef,size(TE,1))
        for i in 1:size(TE,1)
            last_xi_0[i]= current_ghost_values[i][2,2]
        end

        inode_value=set_up_node_values(current_solution,[-1;-1;-1])
        ghosti=compute_ghost_cell_values(inode_value,TE,last_xi_0,Δt,Δx,"current",i_middle((j-1)*Δt),i_middle((j-1)*Δt-Δt))

        ghost_and_values=fill_in_ghost_cells(ghost_and_values,[-1;-1;-1],ghosti)

        for i in 1:size(TE,1)
            solution[i][:,:,j+1]=compute_next_step(C[i],M[i],ghost_and_values[i],solution[i][:,:,j+1])
        end
        
    end

    i_test_num=zeros(J,1)
    for j in 1:J
        for k in 1:size(TE,1)
            for i in 1:2
                i_test_num[j]+=1.5*solution[k][i,end,j]-0.5*solution[k][i,end-1,j]
            end
        end
        # i_test_num[j]-=i_middle((j-1)*Δt)
    end
    
    
    return solution,exact_solution,Δt,TE,TEs,i_test,i_test_num,i_middle

end

end
