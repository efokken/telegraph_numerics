export Node
export TransmissionLine
export Network
export PeriodicNetwork
export PeriodicNode
export PeriodicLine
export ExactNetworkSolution
export Line
export Network
export Discretized_Network
export SingleLine
export Construct_Network
export Line2PeriodicLine
export make_Y
export PeriodicBoundaryCondition
export GeneralBoundaryCondition
export make_Network_discretized
export compute_exact_network_solution
export set_initial_condition
export vector_of_line_solutions
export compute_numerical_solution
export example_solution
export lyapunov_values
export test_lyapunov_property
export lyapunov_to_file

import SparseArrays


abstract type BoundaryCondition end

struct GeneralBoundaryCondition <: BoundaryCondition
    func::Function
end

function(g::GeneralBoundaryCondition)(t)
    return g.func(t)
end


struct PeriodicBoundaryCondition <: BoundaryCondition
    ω::Float64
    coefficient::Complex{Float64}
end

function (p::PeriodicBoundaryCondition)(t)
    return real(p.coefficient*exp(p.ω*t*im))
end





struct PeriodicNode
    Nodecondition::PeriodicBoundaryCondition
    Ntype::String
end

struct PeriodicLine
    Y::Complex{Float64}
    γl::Complex{Float64}
end





struct Node
    Nodecondition::BoundaryCondition
    Ntype::String
end

struct Line
    R::Float64
    L::Float64
    G::Float64
    C::Float64
    l::Float64
    #Line(R,L,G,C,l) = !(R>=0&&G>=0&&L>0&&C>0&&l>0) ? error("parameters not greater/equal 0") : new(R,L,G,C,l)
end

mutable struct SingleLine
    line::Line
    StartNode::Int64
    EndNode::Int64
end

struct Network
    T::Float64
    Nodes::Union{Vector{Node},Vector{PeriodicNode}} #Nodes shall either all be periodic or all be general
    lines::SparseArrays.SparseMatrixCSC{Line,Int64}
end

mutable struct DiscretizedLine
    line::Line
    Δx::Float64
    Solution::Array{Float64,3}
    C::Float64
    λ::Float64
    B::StaticArrays.SMatrix{2,2}
    U::StaticArrays.SMatrix{2,2}

end

#tested
function node_matrices(node,i,linematrix,starting_lines,ending_lines,nattached_lines)
    a=Vector{Float64}(undef,nattached_lines)
    b=Vector{Float64}(undef,nattached_lines)
    λ=Vector{Float64}(undef,nattached_lines)
    c=Vector{Float64}(undef,nattached_lines)
    ΔX=Vector{Float64}(undef,nattached_lines)


    k=1
    for l in 1:length(starting_lines)
        a[k]=starting_lines[l].B[1,1]
        b[k]=starting_lines[l].B[1,2]
        λ[k]=starting_lines[l].λ
        c[k]=sqrt(starting_lines[l].line.L/starting_lines[l].line.C)
        ΔX[k]=starting_lines[l].Δx
        k+=1
    end
    for l in 1:length(ending_lines)
        a[k]=ending_lines[l].B[1,1]
        b[k]=ending_lines[l].B[1,2]
        λ[k]=ending_lines[l].λ
        c[k]=sqrt(ending_lines[l].line.L/ending_lines[l].line.C)
        ΔX[k]=ending_lines[l].Δx
        k+=1
    end
    α= LinearAlgebra.Diagonal(a)
    β= LinearAlgebra.Diagonal(b)
    Λ= LinearAlgebra.Diagonal(λ)

    if(node.Ntype=="current")
        if(nattached_lines==1)
            U=Array{Float64,2}(undef,1,1)
            U[1,1]=-1.0
            # display(U)
            M=Array{Float64,2}(undef,1,1)
            M[1,1]=1.0
            # display(M)
        else
            S = LinearAlgebra.Diagonal([-1 ; ones(nattached_lines-1)])
            M = zeros(nattached_lines,nattached_lines)
            M[1,:]= ones(1,nattached_lines)
            M -= LinearAlgebra.Diagonal([0;c[2:end]])
            M += LinearAlgebra.diagm(-1=>c[1:end-1])
            U=inv(M)*S*M
        end
    else #node.Ntype=="voltage"
        M=LinearAlgebra.Diagonal(c)
        U=LinearAlgebra.I(nattached_lines)
    end
    # display(M)
    M_inv=inv(M)

    return α,β,Λ,U,M_inv,ΔX
end


mutable struct DiscretizedPeriodicNode
    node::PeriodicNode
    α::Array{Float64,2}
    β::Array{Float64,2}
    Λ::Array{Float64,2}
    U::Array{Float64,2}
    M_inv::Array{Float64,2}
    ΔX::Vector{Float64}
    ξ⁻_old::Vector{Float64}
    ξ⁻_oldold::Vector{Float64}
end

function DiscretizedPeriodicNode(node,i,linematrix)

    rowsstart,starting_lines=SparseArrays.findnz(linematrix[i,:])
    rowsend,ending_lines=SparseArrays.findnz(linematrix[:,i])

    nattached_lines=length(starting_lines)+length(ending_lines)

    α,β,Λ,U,M_inv,ΔX = node_matrices(node,i,linematrix,starting_lines,ending_lines,nattached_lines)
    return DiscretizedPeriodicNode(node,α,β,Λ,U,M_inv,ΔX,zeros(nattached_lines),zeros(nattached_lines))

end




mutable struct DiscretizedNode
    node::Node
    α::Array{Float64,2}
    β::Array{Float64,2}
    Λ::Array{Float64,2}
    U::Array{Float64,2}
    M_inv::Array{Float64,2}
    ΔX::Vector{Float64}
    ξ⁻_old::Vector{Float64}
    ξ⁻_oldold::Vector{Float64}
end


function DiscretizedNode(node,i,linematrix)

    rowsstart,starting_lines=SparseArrays.findnz(linematrix[i,:])
    rowsend,ending_lines=SparseArrays.findnz(linematrix[:,i])

    nattached_lines=length(starting_lines)+length(ending_lines)

    α,β,Λ,U,M_inv,ΔX = node_matrices(node,i,linematrix,starting_lines,ending_lines,nattached_lines)

    return DiscretizedNode(node,α,β,Λ,U,M_inv,ΔX,zeros(nattached_lines),zeros(nattached_lines))

end



mutable struct Discretized_Network
    J::Int64
    Δt::Float64
    DiscretizedNodes::Union{Vector{DiscretizedNode},Vector{DiscretizedPeriodicNode}} #Nodes shall either all be periodic or all be general
    DiscretizedLines::SparseArrays.SparseMatrixCSC{DiscretizedLine,Int64}
end


#remains to be tested!
function make_Network_discretized(net,Δx_desired,CFL)

    @assert(0<=CFL<=1)
    @assert(0<Δx_desired)
    rows,columns,lines = SparseArrays.findnz(net.lines)

    #  M=Vector{Int64}(undef,length(lines))
    #  λ=Vector{Float64}(undef,length(lines))
    # ΔX=Vector{Float64}(undef,length(lines))
    M=zeros(Int64,length(lines))
    λ=zeros(length(lines))
    ΔX=zeros(length(lines))

    for i in 1:length(lines)
        M[i]=Int(ceil(lines[i].l/Δx_desired+1))
        ΔX[i] = lines[i].l/(M[i]-1)
        λ[i]=1.0/sqrt(lines[i].L*lines[i].C)
    end

    Δt_candidate = CFL*minimum(ΔX./λ)
    J=Int(ceil(net.T/Δt_candidate)+1)
    Δt = net.T/(J-1)
    dlines=Vector{DiscretizedLine}(undef,length(lines))
    for i in 1:length(lines)
        C=Δt/ΔX[i]*λ[i]
        c=lines[i].R/lines[i].L+lines[i].G/lines[i].C
        b=lines[i].R/lines[i].L-lines[i].G/lines[i].C
        B=1/2*[c b;b c]
        U=exp(-0.5*Δt*B)
        dlines[i]=DiscretizedLine(lines[i],ΔX[i],Array{Float64,3}(undef,2,M[i]+2,J),C,λ[i],B,U)
    end

    DiscretizedLines=SparseArrays.sparse(rows,columns,dlines,size(net.lines,1),size(net.lines,2))
    if(typeof(net.Nodes[1]) == Node)
        DNodes=Vector{DiscretizedNode}(undef,length(net.Nodes))
        for i in 1:length(net.Nodes)
            DNodes[i]=DiscretizedNode(net.Nodes[i],i,DiscretizedLines)
        end
    else #(typeof(net.Nodes[1]) == PeriodicNode)
        DNodes=Vector{DiscretizedPeriodicNode}(undef,length(net.Nodes))
        for i in 1:length(net.Nodes)
            DNodes[i]=DiscretizedPeriodicNode(net.Nodes[i],i,DiscretizedLines)
        end
    end

    return Discretized_Network(J,Δt,DNodes,DiscretizedLines)
end




Base.zero(::Type{Line}) = Line(0,1,0,1,NaN)#Line(0,1,0,1,Inf)
Base.zero(::Line) = Line(0,1,0,1,NaN)#Line(0,1,0,1,Inf)
Base.zero(::Type{PeriodicLine}) = PeriodicLine(0,1)#Line(0,1,0,1,Inf)
Base.zero(::PeriodicLine) = PeriodicLine(0,Inf)#Line(0,1,0,1,Inf)
# Base.zero(::Type{DiscretizedLine}) = DiscretizedLine(zero(Line),0,Array{Float64,3}(undef,2,30,17),0,0,zeros(2,2),[1 0;0 1])
# Base.zero(::DiscretizedLine) = DiscretizedLine(zero(Line),0,Array{Float64,3}(undef,0,0,0),0,0,zeros(,[1 0;0 1])


#tested
function Line2PeriodicLine(Li,ω)
    if(iszero(Li))
        return zero(PeriodicLine)
    else

        W = sqrt(Li.G +im*ω*Li.C)
        Z = sqrt(Li.R +im*ω*Li.L)
        Y = W/Z
        γl = W*Z*Li.l

        return PeriodicLine(Y,γl)
    end
end




#tested:
function Construct_Network(Nodes,SingleLines,T)

    @assert(typeof(SingleLines) == Array{SingleLine,1})
    @assert(typeof(Nodes) == Array{Node,1} || typeof(Nodes) == Array{PeriodicNode,1})
    @assert(T>=0)
    lines=SparseArrays.spzeros(Line,length(Nodes),length(Nodes))
    for i in 1:length(SingleLines)
        if( !(SingleLines[i].line == zero(Line)) && SingleLines[i].StartNode !=0 && SingleLines[i].EndNode !=0 )
            lines[SingleLines[i].StartNode,SingleLines[i].EndNode] = SingleLines[i].line
        end
    end

    return Network(T,Nodes,lines)

end


function make_Y(net,ω)
    @assert typeof(net.Nodes) == Vector{PeriodicNode} "Admittance matrix is only useful for periodic boundary conditions."
    nnodes=length(net.Nodes)
    Y=SparseArrays.spzeros(Complex{Float64},nnodes,nnodes)

    rows, columns, lines = SparseArrays.findnz(net.lines)

    for i in 1:length(rows)
        Pline=Line2PeriodicLine(lines[i],ω)
        Y[rows[i],rows[i]]       +=  Pline.Y/tanh(Pline.γl)
        Y[columns[i],columns[i]] +=  Pline.Y/tanh(Pline.γl)
        Y[rows[i],columns[i]]     = -Pline.Y/sinh(Pline.γl)
        Y[columns[i],rows[i]]     = -Pline.Y/sinh(Pline.γl)
    end

    return Y

end


function Prescribed_voltages_from_Network(net,ω)

    @assert typeof(net.Nodes) == Array{PeriodicNode,1} "cant compute voltages from non-periodic node conditions..."
    nnodes=length(net.Nodes)

    known_voltages = SparseArrays.spzeros(Complex{Float64},nnodes)
    known_currents = SparseArrays.spzeros(Complex{Float64},nnodes)
    for i in 1:nnodes
        if(net.Nodes[i].Ntype == "voltage")
            known_voltages[i]=net.Nodes[i].Nodecondition.coefficient
        else
            known_currents[i]=net.Nodes[i].Nodecondition.coefficient
        end
    end
    c_indices, currents = SparseArrays.findnz(known_currents)
    v_indices, voltages = SparseArrays.findnz(known_voltages)

    P= SparseArrays.sparse([1:length(c_indices);],c_indices,ones(length(c_indices)),length(c_indices),nnodes)
    Y = make_Y(net,ω)


    relevant_currents = currents - P*Y*known_voltages

    missing_voltages=P*Y*transpose(P)\relevant_currents
    return all_voltages=Vector(SparseArrays.sparsevec([c_indices;v_indices],[missing_voltages; voltages]))

end


function compute_line_steady_function(line,ω, V0, V1)

    pline = Line2PeriodicLine(line,ω)

    #complex voltage:
    @inline function V(x)
        return 1/sinh(pline.γl)*(V1*sinh(pline.γl/line.l*x)+V0*sinh(pline.γl/line.l*(line.l-x)))
    end

    #complex current:
    @inline function I(x)
        return -pline.Y/sinh(pline.γl)*(V1*cosh(pline.γl/line.l*x)-V0*cosh(pline.γl/line.l*(line.l-x)))
    end

    #real voltage:
    function v(x,t)
        return real(V(x)*exp(im*ω*t))
    end

    #real current:
    function i(x,t)
        return real(I(x)*exp(im*ω*t))
    end

    function ξ⁺(x,t)
        return 0.5*(i(x,t)+sqrt(line.C/line.L)*v(x,t))
    end

    function ξ⁻(x,t)
        return 0.5*(i(x,t)-sqrt(line.C/line.L)*v(x,t))
    end

    return v,i,ξ⁺,ξ⁻
end

#creates array with exact solution
function compute_exact_line_solution(dline,V0,V1,ω,J,Δt)

    v,i,ξ⁺,ξ⁻ =  compute_line_steady_function(dline.line,ω, V0, V1)

    @inbounds for j in 1:J
        dline.Solution[:,1,j]=[0;0]
        dline.Solution[:,end,j]=[0;0]
        @inbounds for n in 2:size(dline.Solution,2)-1
            dline.Solution[1,n,j]=ξ⁺((n-2)*dline.Δx,(j-1)*Δt)
            dline.Solution[2,n,j]=ξ⁻((n-2)*dline.Δx,(j-1)*Δt)
        end
    end
end

function compute_exact_network_solution(net,dnet,ω)

    @assert typeof(dnet.DiscretizedNodes) == Vector{DiscretizedPeriodicNode} "Exact solution is only computable for periodic boundary conditions."

    rows, columns, dlines = SparseArrays.findnz(dnet.DiscretizedLines)

    voltages=Prescribed_voltages_from_Network(net,ω)

    for i in 1:length(dlines)
        compute_exact_line_solution(dlines[i],voltages[rows[i]],voltages[columns[i]],ω,dnet.J,dnet.Δt)
    end

end

function set_initial_condition(net,dnet,ω)

    rows, columns, dlines = SparseArrays.findnz(dnet.DiscretizedLines)
    voltages=Prescribed_voltages_from_Network(net,ω)

    for l in 1:length(dlines)

        _,_,ξ⁺,ξ⁻ = compute_line_steady_function(dlines[l].line,ω, voltages[rows[l]], voltages[columns[l]])
        for n in 2:size(dlines[l].Solution,2)-1
            dlines[l].Solution[1,n,1]=ξ⁺((n-2)*dlines[l].Δx,0)
            dlines[l].Solution[2,n,1]=ξ⁻((n-2)*dlines[l].Δx,0)
        end
        dlines[l].Solution[:,1,1]=left_extrapolation(dlines[l].Solution[:,2,1],dlines[l].Solution[:,3,1],dlines[l].Solution[:,4,1])
        dlines[l].Solution[:,end,1]=right_extrapolation(dlines[l].Solution[:,end-3,1],dlines[l].Solution[:,end-2,1],dlines[l].Solution[:,end-1,1])
    end
    return nothing
end


function get_difference_of_solutions(dnet1,dnet2)

    dnetdiff=deepcopy(dnet1)

    rows,columns,dlinesdiff=SparseArrays.findnz(dnetdiff.DiscretizedLines)
    rows,columns,dlines1=SparseArrays.findnz(dnet1.DiscretizedLines)
    rows,columns,dlines2=SparseArrays.findnz(dnet2.DiscretizedLines)
    for i in 1:length(dlinesdiff)
        dlinediff=dlinesdiff[i]
        dline1=dlines1[i]
        dline2=dlines2[i]
        dlinediff.Solution=dline1.Solution-dline2.Solution
    end

    return dnetdiff

end

function vector_of_line_solutions(dnet)

    rows,columns,dlines=SparseArrays.findnz(dnet.DiscretizedLines)

    solutions=Array{Array{Float64,3}}(undef,0)

    for i in 1:length(dlines)
        push!(solutions,dlines[i].Solution[:,2:end-1,:])
    end

    return solutions

end

@inline function get_starting_xi_minus(dline,j)
    return dline.Solution[2,2,j]
end

@inline function get_ending_xi_minus(dline,j)
    return -dline.Solution[1,end-1,j]
end

@inline function get_starting_xi_minus_m1(dline,j)
    return dline.Solution[2,1,j]
end

@inline function get_ending_xi_minus_m1(dline,j)
    return -dline.Solution[1,end,j]
end




function set_node_ghostcells(dnet,i,j)

    rowsstart,_=SparseArrays.findnz(dnet.DiscretizedLines[i,:])
    rowsend,_=SparseArrays.findnz(dnet.DiscretizedLines[:,i])
    nattached_lines=length(rowsstart)+length(rowsend)


    if(dnet.DiscretizedNodes[i].node.Ntype=="voltage")
        aux=ones(nattached_lines)
    else #(dnet.DiscretizedNodes[i].node.Ntype=="current")
        aux=zeros(nattached_lines)
        aux[1]=1.0
    end
    node_vector=dnet.DiscretizedNodes[i].node.Nodecondition((j-1)*dnet.Δt)*aux


    #ξ⁻=Vector{Float64}(undef,nattached_lines)
    ξ⁻=zeros(nattached_lines)
    # ξ⁻_m1=zeros(nattached_lines)
    k=1
    for l in 1:length(rowsstart)
        ξ⁻[k]=get_starting_xi_minus(dnet.DiscretizedLines[i,rowsstart[l]],j)
        # ξ⁻_m1[k]=get_starting_xi_minus_m1(dnet.DiscretizedLines[i,rowsstart[l]],j)
        k+=1
    end
    for l in 1:length(rowsend)
        ξ⁻[k]=get_ending_xi_minus(dnet.DiscretizedLines[rowsend[l],i],j)
        # ξ⁻_m1[k]=get_ending_xi_minus_m1(dnet.DiscretizedLines[rowsend[l],i],j)
        k+=1
    end

    # display(ξ⁻)


    # display(dnet.DiscretizedNodes[i].M_inv)
    ξ⁺=dnet.DiscretizedNodes[i].U*ξ⁻+dnet.DiscretizedNodes[i].M_inv*node_vector

    # dξ⁻dt=derivative(ξ⁻,dnet.DiscretizedNodes[i].ξ⁻_old,dnet.DiscretizedNodes[i].ξ⁻_oldold,dnet.Δt)
    # # dξ⁺dt=(ξ⁺-dnet.DiscretizedNodes[i].ξ⁺_old) /dnet.Δt

    # dnodevvectordt=der(dnet.DiscretizedNodes[i].node.Nodecondition,(j-1)*dnet.Δt)*aux
    # dξ⁺dt=dnet.DiscretizedNodes[i].U*dξ⁻dt+dnet.DiscretizedNodes[i].M_inv*dnodevvectordt


    # #For voltage nodes, this is very exact
    # if(dnet.DiscretizedNodes[i].node.Ntype=="voltage")
    #     v_now=dnet.DiscretizedNodes[i].node.Nodecondition((j-1)*dnet.Δt)
    #     # v_old=dnet.DiscretizedNodes[i].node.Nodecondition((j-2)*dnet.Δt)
    #     # v_oldold=dnet.DiscretizedNodes[i].node.Nodecondition((j-3)*dnet.Δt)
    #     dvdt=der(dnet.DiscretizedNodes[i].node.Nodecondition,(j-1)*dnet.Δt)
    #     iprime=Vector{Float64}(undef,nattached_lines)
    #     k=1
    #     for l in 1:length(rowsstart)
    #         iprime[k]= -dnet.DiscretizedLines[i,rowsstart[l]].line.G*v_now-dnet.DiscretizedLines[i,rowsstart[l]].line.C*dvdt
    #         k+=1
    #     end
    #     for l in 1:length(rowsend)
    #         iprime[k]= -dnet.DiscretizedLines[rowsend[l],i].line.G*v_now-dnet.DiscretizedLines[rowsend[l],i].line.C*dvdt
    #         k+=1
    #     end
    #     i_now=ξ⁺+ξ⁻
    #     i_m1 = i_now -dnet.DiscretizedNodes[i].ΔX .* iprime

    #     ξ⁺_m1=-ξ⁻_m1 + i_m1
    # end


    # #for current nodes we try something different:

    # if(dnet.DiscretizedNodes[i].node.Ntype=="current")
    #     R=Vector{Float64}(undef,nattached_lines)
    #     L=Vector{Float64}(undef,nattached_lines)
    #     c=Vector{Float64}(undef,nattached_lines)

    #     k=1
    #     for l in 1:length(rowsstart)
    #         R[k]=dnet.DiscretizedLines[i,rowsstart[l]].line.R
    #         L[k]=dnet.DiscretizedLines[i,rowsstart[l]].line.L
    #         c[k]=sqrt(dnet.DiscretizedLines[i,rowsstart[l]].line.L/dnet.DiscretizedLines[i,rowsstart[l]].line.C)
    #         k+=1
    #     end
    #     for l in 1:length(rowsend)
    #         R[k]=dnet.DiscretizedLines[rowsend[l],i].line.R
    #         L[k]=dnet.DiscretizedLines[rowsend[l],i].line.L
    #         c[k]=sqrt(dnet.DiscretizedLines[rowsend[l],i].line.L/dnet.DiscretizedLines[rowsend[l],i].line.C)

    #         k+=1
    #     end
    #     RR=LinearAlgebra.Diagonal(R)
    #     LL=LinearAlgebra.Diagonal(L)
    #     C=LinearAlgebra.Diagonal(c)
    #     DX=LinearAlgebra.Diagonal(dnet.DiscretizedNodes[i].ΔX)

    #     ξ⁺_m1=ξ⁻_m1 +(LinearAlgebra.I(nattached_lines)+inv(C)*DX*RR)*ξ⁺+inv(C)*DX*LL*(dξ⁻dt+dξ⁺dt)

    # end


    #setup the next old_value:
    # dnet.DiscretizedNodes[i].ξ⁻_oldold=dnet.DiscretizedNodes[i].ξ⁻_old
    # dnet.DiscretizedNodes[i].ξ⁻_old=ξ⁻
        # display(dnet.DiscretizedNodes[i].ΔX)
    # display(ξ⁺)
    # display(ξ⁺_m1)

    k=1
    for l in 1:length(rowsstart)
        fill_in_startingline_ghost_cells(dnet.DiscretizedLines[i,rowsstart[l]],ξ⁺[k],j)
        k+=1
    end
    for l in 1:length(rowsend)
        fill_in_endingline_ghost_cells(dnet.DiscretizedLines[rowsend[l],i],ξ⁺[k],j)
        k+=1
    end


    return nothing

end

@inline function derivative(a,b,c,Δt)
    return (3.0*a-4.0*b+c)/(2.0*Δt)
end


@inline function fill_in_startingline_ghost_cells(dline,ξ⁺,j)
    dline.Solution[1,2,j]=ξ⁺
    return nothing
end

@inline function fill_in_endingline_ghost_cells(dline,ξ⁺,j)
    dline.Solution[2,end-1,j]=-ξ⁺
    return nothing
end





@inline function set_ghostcells(dnet,j)
    for i in 1:length(dnet.DiscretizedNodes)
        set_node_ghostcells(dnet,i,j)
    end
    return nothing
end


@inline function step_line(dline,j)

    dline.Solution[:,1,j-1] = left_extrapolation(dline.Solution[:,2,j-1],dline.Solution[:,3,j-1],dline.Solution[:,4,j-1])
    dline.Solution[:,end,j-1]  =right_extrapolation(dline.Solution[:,end-4,j-1],dline.Solution[:,end-2,j-1],dline.Solution[:,end-1,j-1])

    dline.Solution[:,3:end-2,j]=compute_next_step(dline.C,dline.U,dline.Solution[:,:,j-1],dline.Solution[:,3:end-2,j])

    #Here extrapolate linearly to fill the ghost cells.
    dline.Solution[2,2,j]=left_extrapolation(dline.Solution[2,3,j],dline.Solution[2,4,j],dline.Solution[2,5,j])
    dline.Solution[1,end-1,j]=right_extrapolation(dline.Solution[1,end-4,j],dline.Solution[1,end-3,j],dline.Solution[1,end-2,j])
    
    return nothing
end



@inline function step_line_solutions(dnet,j)

    rows,columns,dlines=SparseArrays.findnz(dnet.DiscretizedLines)

    for i in 1:length(dlines)
        step_line(dlines[i],j)
    end
    return nothing
end

function compute_numerical_solution(dnet)

    for j in 2:dnet.J
        set_ghostcells(dnet,j-1)
        step_line_solutions(dnet,j)
    end
    set_ghostcells(dnet,dnet.J)

    return nothing
end




function example_solution(n)

    Δx_desired=2.0^(-n)
    CFL=0.8


    net,ω=dummy_periodic_net()
    # net,ω=dummy_line()
    dnet=make_Network_discretized(net,Δx_desired,CFL)

    set_initial_condition(net,dnet,ω)

    compute_numerical_solution(dnet)

    exactdnet=deepcopy(dnet)
    compute_exact_network_solution(net,exactdnet,ω)

    diffnet=get_difference_of_solutions(dnet,exactdnet)


    # _,_,lines=SparseArrays.findnz(dnet.DiscretizedLines)


    # return test_boundary_conditions(dnet)
    return dnet,exactdnet,diffnet
    # return vector_of_line_solutions(dnet)

end




function create_lyapunov_test_network()
    net,ω =dummy_periodic_net()

    N1 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"current")
    N2 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"voltage")
    N3 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"voltage")
    N4 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"voltage")
    Ns=[N1;N2;N3;N4]

    Li1 = Line(2,6,2,1,2)
    Li2 = Line(3,6,1,1,2)
    Li3 = Line(1,9,2,1,2)




    # N1 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"current")
    # N2 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"voltage")
    # N3 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"voltage")
    # N4 = PeriodicNode(PeriodicBoundaryCondition(ω,0.0),"voltage")
    # Ns=[N1;N2;N3;N4]

    # Li1 = Line(4,6,2,1,4)
    # Li2 = Line(4,6,8,1,5)
    # Li3 = Line(4,9,8,1,10)
    SiLi1=SingleLine(Li1,1,2)
    SiLi2=SingleLine(Li2,1,3)
    SiLi3=SingleLine(Li2,1,4)
    SiLis=[SiLi1;SiLi2;SiLi3]
    T=3.0

    zero_net=Construct_Network(Ns,SiLis,T)

    return net, zero_net, ω

end


function test_lyapunov_property(n)

    Δx_desired=2.0^(-n)
    CFL=0.8

    net , zero_net , ω = create_lyapunov_test_network()

    dnet     =make_Network_discretized(net,Δx_desired,CFL)
    zero_dnet=make_Network_discretized(zero_net,Δx_desired,CFL)

    set_initial_condition(net,zero_dnet,ω)


    compute_numerical_solution(zero_dnet)

    return zero_dnet
end



function lyapunov_values(dnet)

    _,_,dlines=SparseArrays.findnz(dnet.DiscretizedLines)

    Lyapunov=Vector{Float64}(undef,dnet.J)
    for j in 1:dnet.J
        for i in 1:length(dlines)
            Lyapunov[j]+=sum(dlines[i].Solution[:,2:end-1,j] .* dlines[i].Solution[:,2:end-1,j])
        end
    end
    return Lyapunov
end


function lyapunov_to_file(n)


    zero_dnet=test_lyapunov_property(n)
    T=zero_dnet.J*zero_dnet.Δt
    A=lyapunov_values(zero_dnet)
    A=A/A[1]

    rows,columns,dlines=SparseArrays.findnz(zero_dnet.DiscretizedLines)
    exponent=Inf
    for k in 1:length(dlines)
        exponent=min(exponent,dlines[k].line.R/dlines[k].line.L,dlines[k].line.G/dlines[k].line.C)
    end

    lyap=t->exp(-2*exponent*t)
    
    B=lyap.(range(0, stop=T,length=length(A)))


    sol=[[0;0] [transpose(A);transpose(B)] [0;0]]

    write_solution_to_file("lyapunov.table",-1,sol,zero_dnet.Δt)

end


