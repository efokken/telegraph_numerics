export compute_solution
export compute_next_step
export compute_ghost_cell_values
export set_up_node_values



@inline function ode_on_five_entries(M,xi)

    return M*xi
end

# "new" limiter:
# @inline function phi(theta)
#     if (theta <= 0)
#         return 0.0
#     elseif(theta>=0.50)
#         return 1.0
#     else
#         return 2*theta
#     end
# end

# ## pure Lax Wendroff:
# @inline function phi(theta)
#         return 1.0
# end


 ##minmod:
@inline function phi(theta)
    if (theta <= 0)
        return 0.0
    elseif(theta>=1.0)
        return 1.0
    else
        return theta
    end
end


@inline function thetaplus(u1m,u0,u1)
    if (u1m == u0 && u0 == u1) #(u1==u0 && u0 == u1m)
        return 1.0
    else
        return (u0-u1m)/(u1-u0)
    end
end

@inline function thetaminus(u1m,u0,u1)

    if (u1m == u0 && u0 == u1) #(u1==u0 && u0 == u1m)
        return 1.0
    else
        return (u1-u0)/(u0-u1m)
    end
end



@inline function limited_scheme(C,a,u)
    if(mydebugging())
        # first assert the dimensions of the input data
        @assert size(u)==(5,)
        #assert CFL condition:
        @assert 0<=C<=1
    end

    if(a>0)
        theta_right= thetaplus(u[2],u[3],u[4])
        theta_left = thetaplus(u[1],u[2],u[3])
        #        println(phi(theta_right))
        return  u[3] - C*(u[3]-u[2]) -0.5*C*(1-C)*(phi(theta_right)*(u[4]-u[3])-phi(theta_left)*(u[3]-u[2]))
    else
        theta_right = thetaminus(u[3],u[4],u[5])
        theta_left = thetaminus(u[2],u[3],u[4])
        return u[3] + C*(u[4]-u[3]) -0.5*C*(1-C)*(phi(theta_right)*(u[4]-u[3])-phi(theta_left)*(u[3]-u[2]))
    end

end



@inline function pde_compute_current_point(C,xi)
    if(mydebugging())
        # first assert the dimensions of the input data
        @assert(size(xi)==(2,5))
        #assert CFL condition:
        @assert(0<=C<=1)
    end

    #Return Vector:
    return StaticArrays.SVector{2}(limited_scheme(C,1,xi[1,:]), limited_scheme(C,-1,xi[2,:]))
end

# @inline function left_extrapolation(ξ₀,ξ₁,ξ₂)
#     a = 0.5*(ξ₀+ξ₂-2.0*ξ₁)
#     b = 0.5*(4.0*ξ₁ -3.0*ξ₀ -ξ₂)
#     c = ξ₀

#     return (-1.0)^2*a+(-1)*b+c
# end

# @inline function right_extrapolation(ξ₋₂,ξ₋₁,ξ₀)
#     a = 0.5*(ξ₀+ξ₋₂-2.0*ξ₋₁)
#     b = 0.5*(4.0*ξ₋₁ -3.0*ξ₀ -ξ₋₂)
#     c = ξ₀

#     return a*(-1.0)^2+b*(-1)+c
# end



@inline function left_extrapolation(ξ₀,ξ₁,ξ₂)
    return 2ξ₀-ξ₁
end


@inline function right_extrapolation(ξ₋₂,ξ₋₁,ξ₀)
    return 2ξ₀-ξ₋₁
end





function compute_next_step(C,M,xi_with_ghost_cells,newxi)
    # This function computes the inner new values
    if(mydebugging())
        #assert input sizes:
        @assert(size(M)==(2,2))
        @assert(0<=C<=1)
        @assert(size(xi_with_ghost_cells,1)==2)
        @assert(size(newxi,1)==2)
        @assert(size(xi_with_ghost_cells,2)==size(newxi,2)+4)
    end

    #Be careful, the following is very entangled to save some computation time. It should be equivalent to:
    # xi_1 = ode(xi_with_ghost_cells)
    # xi_2 = pde(xi_1)  (here the ghost cells are used and then discarded)
    # newxi= ode(xi_2)
    for i in 3:size(newxi,2)+2
        newxi[:,i-2]=ode_on_five_entries(M,pde_compute_current_point(C,ode_on_five_entries(M,StaticArrays.SMatrix{2,5}(view(xi_with_ghost_cells,:,i-2:i+2)))))
    end

    return newxi
end


function compute_ghost_cell_values(xi_minus_on_lines,TE,xi_minus_0_old,Δt,Δx,type,node_value,node_value_old)

    if(mydebugging())
        @assert(size(xi_minus_on_lines,2) ==2)
        @assert(size(xi_minus_on_lines,1) == size(TE,1) == size(xi_minus_0_old,1))
    end

    number_of_lines=size(xi_minus_on_lines,1)

    λ = Vector{Float64}(undef,number_of_lines)
    for i in 1:number_of_lines
        λ[i] = 1/sqrt(TE[i].L*TE[i].C)
    end
    Λ = LinearAlgebra.Diagonal(λ)

    α = Vector{Float64}(undef,number_of_lines)
    β = Vector{Float64}(undef,number_of_lines)
    for i in 1:number_of_lines
        α[i] = TE[i].R/TE[i].L+TE[i].G/TE[i].C
    end
    for i in 1:number_of_lines
        β[i] = TE[i].R/TE[i].L-TE[i].G/TE[i].C
    end
    A= LinearAlgebra.Diagonal(α)
    B= LinearAlgebra.Diagonal(β)

    c = Vector{Float64}(undef,number_of_lines)
    for i in 1:number_of_lines
        c[i] = sqrt(TE[i].L/TE[i].C)
    end

    xi_minus_0 = 1.5*xi_minus_on_lines[:,1]-0.5*xi_minus_on_lines[:,2]

    #current:
    if(type == "voltage")
        node_vector = node_value * ones(number_of_lines,1)
        dnode_vector_dt = (node_value-node_value_old)/Δt * ones(number_of_lines,1)

        S = LinearAlgebra.I(number_of_lines)
        M = LinearAlgebra.Diagonal(c)
        # println(M)
        U = inv(M)*S*M

    elseif(type=="current")
        node_vector = zeros(number_of_lines,1)
        dnode_vector_dt = zeros(number_of_lines,1)
        node_vector[1] = node_value
        dnode_vector_dt[1] = (node_value-node_value_old)/Δt

        if(number_of_lines==1)
            S=[-1]
            M=[1]
        else
            S = LinearAlgebra.Diagonal([-1 ; ones(number_of_lines-1,1)])
            M = zeros(number_of_lines,number_of_lines)
            M[1,:]= ones(1,number_of_lines)
            M = M - LinearAlgebra.Diagonal([0;c[2:end]])
            M = M + LinearAlgebra.diagm(-1=>c[1:end-1])
        end
        U = inv(M)*S*M


    end

    xi_plus_0 = U*xi_minus_0 +inv(M)*node_vector

    dxi_minus_0_dt = (xi_minus_0-xi_minus_0_old)/Δt
    dxi_plus_0_dx = -inv(Λ)*(A*xi_plus_0+ B*xi_minus_0 +U*dxi_minus_0_dt- inv(M)*dnode_vector_dt)

    #for test purposes constant extrapolation
    xi_plus_m1 = xi_plus_0-Δx*dxi_plus_0_dx

    ghost_cells=Vector{Array{Float64,2}}(undef,number_of_lines)
    for i in 1:number_of_lines
        ghost_cells[i]=[[xi_plus_m1[i];0] [xi_plus_0[i];xi_minus_0[i]]]
    end



    return ghost_cells

end

function set_up_node_values(xi_vector,orientation)
    number_of_lines=size(xi_vector,1)
    if(mydebugging())
        @assert(number_of_lines==size(orientation,1))
    end
    
    node_values=Array{Float64,2}(undef,number_of_lines,2)

    for i in 1:number_of_lines
        if(orientation[i]==1)
            node_values[i,:]=xi_vector[i][2,1:2]
        elseif(orientation[i]==-1)
            node_values[i,:]=-xi_vector[i][1,end:-1:end-1]
        else
            println("Error! Orientation may only contain +/- 1.")
        end
    end
    return node_values
end



function fill_in_ghost_cells(ghost_and_values,orientation,ghost_cells)

    number_of_lines= size(ghost_cells,1)
    if(mydebugging())
        @assert(number_of_lines==size(orientation,1))
        @assert(number_of_lines==size(ghost_and_values,1))
    end

    P=StaticArrays.SMatrix{2,2}([0 1;1 0])

    for i in 1:number_of_lines
        if(orientation[i]==1)
            ghost_and_values[i][:,1:2] = ghost_cells[i]
        elseif(orientation[i]==-1)
            ghost_and_values[i][:,end-1:end] = -P*ghost_cells[i]*P
        else
            println("Error! Orientation may only contain +/- 1.")
        end
    end
    return ghost_and_values
end


# This function computes the solution over time T on length X with a space discretization of n points, including the boundaries.
# The time step is chosen so that the CFL condition always is 0.8.
function compute_solution(Tele,Δx,CFL_desired,init,v,i)


    @assert Tele.R>=0 "Resistance is negative!"
    @assert Tele.L>0 "Inductance is negative or zero!"
    @assert Tele.G>=0 "Admittance is negative!"
    @assert Tele.C>0 "Capacitance is negative or zero!"
    @assert Tele.l>0 "line length is negative or zero!"
    @assert Tele.T>=0 "Integration time is negative!"
    @assert 0<=CFL_desired<=1 "CFL number must be between 0 and 1!"

    #compute the number of spatial points:
    N=Int(round(Tele.l/Δx+1))
    @assert isapprox((N-1)*Δx,Tele.l) "The spatial stepsize Δx is wrong: l/Δx is not an integer."


    char_init=char_fx(init,Δx)

    char_v=char_fxt(v,Δx)
    char_i=char_fxt(i,Δx)

    function char_v_0(t)
        return char_v(0,t)
    end
    function char_v_l(t)
        return char_v(Tele.l,t)
    end

    function char_i_0(t)
        return char_i(0,t)
    end
    function char_i_l(t)
        return char_i(Tele.l,t)
    end


    # compute step sizes:
    a=1/sqrt(Tele.L*Tele.C)

    Δt_cfl=CFL_desired*Δx/a





    #compute coefficients of the Telegrapher's equation in characteristic variables
    c=Tele.R/Tele.L+Tele.G/Tele.C

    b=Tele.R/Tele.L-Tele.G/Tele.C

    B=1/2*[c b;b c]

    eigenvalues=LinearAlgebra.eigen(B).values
    #to get an integer number of timesteps we make Δt slightly smaller so that T/Δt in Z
    Δt_ode_one=2/eigenvalues[1]
    Δt_ode_two=2/eigenvalues[2]
    Δt_dash=min(Δt_ode_one,Δt_ode_two,Δt_cfl)


    J=Int(ceil(Tele.T/Δt_dash)+1)
    Δt=Tele.T/(J-1)
    Printf.@printf("Δt = %.6e\n",Δt)
    #compute CFL number
    CFL=Δt/Δx*a

    #compute ode step matrix (mind that we only do the half step!)
    M=StaticArrays.SMatrix{2,2}([1 0;0 1]-0.5*Δt*B+(1/8)*Δt^2*B^2)


    solution=Array{Float64,3}(undef,2,N,J)

    #set initial values:
    for i in 1:N
        solution[:,i,1] = char_init((i-1)*Δx)
    end

    #first step different because of the current information:
    i_last_0=char_i(0.0,0)
    i_last_l=char_i(Tele.l,0)

    compute_next_step(a,0.0,Δt,Δx,CFL,M,view(solution,:,:,1),view(solution,:,:,2),char_v_0,char_v_l,i_last_0,i_last_l,Tele)

    #rest of the steps:
    @inbounds for i in 2:J-1
        i_last_0=solution[1,1,i-1]+solution[2,1,i-1]
        i_last_l=solution[1,end,i-1]+solution[2,end,i-1]
        compute_next_step(a,(i-1)*Δt,Δt,Δx,CFL,M,view(solution,:,:,i),view(solution,:,:,i+1),char_v_0,char_v_l,i_last_0,i_last_l,Tele)
    end
    return solution, Δt, Δx,N,J

end
