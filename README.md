# telegraph numerics

This is a collection of function that showcases a split scheme for integrating the Telegrapher's equations on a network as described in out paper.

To use it one needs a julia binary and must import the Telegraphers package provided in this repository.



Then running

```
ld=10; B=Vector{Float64}(undef,ld);d=Vector{Discretized_Network}(undef,0);e=Vector{Discretized_Network}(undef,0); for i in 1:ld _,ex,dx=example_solution(i); push!(d,dx); push!(e,ex);  end; for i in 1:ld sol=vector_of_line_solutions(d[i]);B[i]=maximum(sol[1][:,2:end-1,:])end;
```
where `ld` is an integer, defining the stepsize `2^-ld`.

```
n=9;show_solution(d[n].DiscretizedLines[1,2].Solution[:,2:end-1,:],d[n].Δt,d[n].DiscretizedLines[1,2].line)
```
where `n` is an integer, defining the stepsize `2^-n`.
This will create a vector `B` of maximal errors on the example network and show errors over time on one of the lines in the network.


To show the actual solution and not its error, replace the second line with
```
n=9;show_solution(e[n].DiscretizedLines[1,2].Solution[:,2:end-1,:],d[n].Δt,d[n].DiscretizedLines[1,2].line)
```


To get a Lyapunov-estimate, you can run

```lyapunov_to_file(n)```

This will output a file of computed Lyapunov values in column xiplus and of an theoretical upper bound on these in column ximinus.


There is also a rudimentary testsuite, the tests of which can be run with `testall()`.
