export test_Line2PeriodicLine
export test_Construct_Network
export test_make_Y
export dummynet
export test_Prescribed_voltages_from_Network
export test_make_Network_discretized
export test_compute_exact_network_solution
export test_set_initial_condition
export test_set_initial_condition
export test_compute_numerical_solution
export test_node_matrices

export testall

function testall()
    display(test_Line2PeriodicLine())
    display(test_Construct_Network())
    display(test_make_Y())
    display(test_Prescribed_voltages_from_Network())
    display(test_make_Network_discretized())
    display(test_compute_exact_network_solution())
    display(test_set_initial_condition())
    display(test_node_matrices())
    display(test_compute_numerical_solution())
end





function dummynet()

    N1 = Node( GeneralBoundaryCondition(t -> sin(1t)),"current")
    N2 = Node( GeneralBoundaryCondition(t -> sin(2t)),"voltage")
    N3 = Node( GeneralBoundaryCondition(t -> sin(3t)),"voltage")
    N4 = Node( GeneralBoundaryCondition(t -> sin(4t)),"voltage")
    Ns=[N1;N2;N3;N4]

    Li1 = Line(4.,6.,2,1,4)
    Li2 = Line(4,6,8,1,3)
    Li3 = Line(4,10,8,1,5)
    SiLi1=SingleLine(Li1,2,1)
    SiLi2=SingleLine(Li2,3,1)
    SiLi3=SingleLine(Li3,4,1)
    SiLis=[SiLi1;SiLi2;SiLi3]
    T=3.0

    return Construct_Network(Ns,SiLis,T)

end


function dummy_periodic_net()
    ω=4.0
    # N1 = PeriodicNode(PeriodicBoundaryCondition(ω,10.0+3.0*im),"voltage")
    N1 = PeriodicNode(PeriodicBoundaryCondition(ω,10.0+3.0*im),"current")
    N2 = PeriodicNode(PeriodicBoundaryCondition(ω,4.0+4.0*im),"voltage")
    N3 = PeriodicNode(PeriodicBoundaryCondition(ω,2.0+5.0*im),"voltage")
    N4 = PeriodicNode(PeriodicBoundaryCondition(ω,3.0+6.0*im),"voltage")
    Ns=[N1;N2;N3;N4]

    Li1 = Line(2,6,2,1,2)
    Li2 = Line(3,6,1,1,2)
    Li3 = Line(1,9,2,1,2)
    SiLi1=SingleLine(Li1,1,2)
    SiLi2=SingleLine(Li2,1,3)
    SiLi3=SingleLine(Li2,1,4)
    SiLis=[SiLi1;SiLi2;SiLi3]
    T=3.0

    return Construct_Network(Ns,SiLis,T), ω
end

function dummy_line()
    ω=4.0
    N1 = PeriodicNode(PeriodicBoundaryCondition(ω,5.0+3.0*im),"voltage")
    N2 = PeriodicNode(PeriodicBoundaryCondition(ω,2.0+5.0*im),"current")
    Ns=[N1;N2]

    Li1 = Line(4,6,2,1,1)
    SiLi1=SingleLine(Li1,1,2)
    SiLis=[SiLi1]
    T=1.0

    return Construct_Network(Ns,SiLis,T), ω
end


function test_Line2PeriodicLine()

    Li=Line(1,2,3,4,5)
    ω=3
    P=Line2PeriodicLine(Li,ω)
    γ=P.γl/Li.l

    G=real(P.Y*γ)
    C=imag(P.Y*γ)/ω
    R=real(γ/P.Y)
    L=imag(γ/P.Y)/ω

    A=[G;C;R;L]
    B=[Li.G;Li.C;Li.R;Li.L]
    return isapprox(A,B,atol=1e-12)

end


function test_Construct_Network()


    N0 = Node(GeneralBoundaryCondition(t -> sin(2t)),"voltage")
    N1 = Node(GeneralBoundaryCondition(t -> sin(2t)),"voltage")
    Ns=[N0;N1]

    Li = Line(4,6,2,1,4)
    SiLi=SingleLine(Li,1,2)
    SiLis=[SiLi]
    T=3.0

    net=Construct_Network(Ns,SiLis,T)


    success = (typeof(net.lines) == SparseArrays.SparseMatrixCSC{Line,Int64})
    success = success && (net.lines[1,2]==Li)&& SparseArrays.nnz(net.lines)==1
    success = success && net.Nodes == Ns

    return success
end


#currently not working, as the net structure is hard-coded, but dummy_periodic_net() was changed...
function test_make_Y()

    net,ω=dummy_periodic_net()

    Y = make_Y(net,ω)


    Pline1 = Line2PeriodicLine(net.lines[1,2],ω)
    Pline2 = Line2PeriodicLine(net.lines[1,3],ω)
    Pline3 = Line2PeriodicLine(net.lines[1,4],ω)

    success1 = Y[1,2] == -Pline1.Y/sinh(Pline1.γl)
    success2 = Y[1,3] == -Pline2.Y/sinh(Pline2.γl)
    success3 = Y[1,1] ==  Pline1.Y/tanh(Pline1.γl)+Pline2.Y/tanh(Pline2.γl)+Pline3.Y/tanh(Pline3.γl)
    success4 = Y[2,2] ==  Pline1.Y/tanh(Pline1.γl)
    success5 = Y==transpose(Y)
    return [success1;success2;success3;success4;success5]

end

function test_Prescribed_voltages_from_Network()

    net,ω=dummy_periodic_net()

    Y = make_Y(net,ω)


    V = Prescribed_voltages_from_Network(net,ω)

    indices=Vector{Int64}(undef,0)
    currents=Vector{Complex{Float64}}(undef,0)
    for i in 1:length(net.Nodes)
        if(net.Nodes[i].Ntype == "current")
            push!(indices,i)
            push!(currents,net.Nodes[i].Nodecondition.coefficient)
        end

    end

    I=Y*V
    success = true
    for i in 1:length(indices)
        success&=isapprox((Y*V)[indices[i]],currents[i],atol=1e-12)
    end

    return isapprox((Y*V)[1],net.Nodes[1].Nodecondition.coefficient,atol=1e-12)
end



#not ready:
function test_make_Network_discretized()

    net =dummynet()

    Δx= 1.0/20.0
    CFL=0.9

    make_Network_discretized(net,Δx,CFL)

    return true

end

function test_boundary_conditions(dnet)

    M=dnet.DiscretizedLines
    nodes=dnet.DiscretizedNodes
    success=true
    successi=true

    for j in 1:dnet.J

        for i in 1:length(nodes)
            if(SparseArrays.nnz(M[i,:])!=0)
                _,startlines= SparseArrays.findnz(M[i,:])
            else
                startlines=[]
            end
            if(SparseArrays.nnz(M[:,i])!=0)
                _,endlines= SparseArrays.findnz(M[:,i])
            else
                endlines=[]
            end

            if (nodes[i].node.Ntype=="voltage")
                # display("correct voltage:")
                # display(nodes[i].Nodecondition((j-1)*dnet.Δt))
                # display("actual voltage:")
                for k in 1:length(startlines)
                    li=startlines[k]
                    v = sqrt(li.line.L/li.line.C)*(li.Solution[1,2,j]-li.Solution[2,2,j])
                    success&=isapprox(nodes[i].node.Nodecondition((j-1)*dnet.Δt),v,atol=1e-12)
                    # display(v)
                end
                for k in 1:length(endlines)
                    li=endlines[k]
                    v = sqrt(li.line.L/li.line.C)*(li.Solution[1,end-1,j]-li.Solution[2,end-1,j])
                    success&=isapprox(nodes[i].node.Nodecondition((j-1)*dnet.Δt),v,atol=1e-12)
                    # display(v)
                end
            else #(nodes[i].node.Ntype=="current")
                current=0.0

                if(length(startlines)>0)
                    li=startlines[1]
                    comp_voltage = sqrt(li.line.L/li.line.C)*(li.Solution[1,2,j]-li.Solution[2,2,j])
                else #(length(startlines)>0)
                    li=endlines[1]
                    comp_voltage=sqrt(li.line.L/li.line.C)*(li.Solution[1,end-1,j]-li.Solution[2,end-1,j])
                end

                for k in 1:length(startlines)
                    li=startlines[k]
                    current+=(li.Solution[1,2,j]+li.Solution[2,2,j])
                    success&=isapprox(comp_voltage,sqrt(li.line.L/li.line.C)*(li.Solution[1,2,j]-li.Solution[2,2,j]),atol=1e-12)
                end
                for k in 1:length(endlines)
                    li=endlines[k]
                    current-=(li.Solution[1,end-1,j]+li.Solution[2,end-1,j])
                    success&=isapprox(comp_voltage,sqrt(li.line.L/li.line.C)*(li.Solution[1,end-1,j]-li.Solution[2,end-1,j]),atol=1e-12)
                end
                # display("correct current:")
                # display(nodes[i].Nodecondition((j-1)*dnet.Δt))
                # display("actual current:")
                # display(current)

                successi&=isapprox(nodes[i].node.Nodecondition((j-1)*dnet.Δt),current,atol=1e-12)
            end

        end

    end
    return success, successi
end





function test_compute_exact_network_solution()

    Δx_desired= 1.0/10.0
    CFL=0.8

    # net,ω=dummy_periodic_net()
    net,ω=dummy_line()
    dnet=make_Network_discretized(net,Δx_desired,CFL)

    compute_exact_network_solution(net,dnet,ω)

    return test_boundary_conditions(dnet)

end


function test_set_initial_condition()
    Δx_desired= 1.0/10.0
    CFL=0.8

    net,ω=dummy_periodic_net()
    # net,ω=dummy_line()
    dnet=make_Network_discretized(net,Δx_desired,CFL)

    set_initial_condition(net,dnet,ω)

    testdnet=dnet
    compute_exact_network_solution(net,testdnet,ω)

    diffnet=get_difference_of_solutions(dnet,testdnet)
    solutions=vector_of_line_solutions(diffnet)

    success=true
    for i in 1:length(solutions)
        success =success && iszero(solutions[i][:,2:end-1,1])
    end
    return success

end

function test_node_matrices()
    Δx_desired= 1.0/10.0
    CFL=0.8

    net,ω=dummy_line()
    dnet=make_Network_discretized(net,Δx_desired,CFL)

    rows,columns,lines=SparseArrays.findnz(dnet.DiscretizedLines)

    # display("start, voltage")
    # comp=Vector{Float64}(undef,)
    comp=[dnet.DiscretizedNodes[1].α[1]-lines[1].B[1,1];
          dnet.DiscretizedNodes[1].β[1]-lines[1].B[1,2];
          dnet.DiscretizedNodes[1].Λ[1,1]-lines[1].λ;
          dnet.DiscretizedNodes[1].U[1,1]-1.0;
          dnet.DiscretizedNodes[1].M_inv[1,1]-1/sqrt(lines[1].line.L/lines[1].line.C);
          dnet.DiscretizedNodes[2].α[1]-lines[1].B[1,1];
          dnet.DiscretizedNodes[2].β[1]-lines[1].B[1,2];
          dnet.DiscretizedNodes[2].Λ[1,1]-lines[1].λ;
          dnet.DiscretizedNodes[2].U[1,1]+1.0;
          # dnet.DiscretizedNodes[2].M_inv[1,1]-1.0
          ]

    return iszero(comp)

end



function test_compute_numerical_solution()
    Δx_desired= 1.0/100.0
    CFL=0.8


    net,ω=dummy_periodic_net()
    # net,ω=dummy_line()
    dnet=make_Network_discretized(net,Δx_desired,CFL)

    set_initial_condition(net,dnet,ω)

    compute_numerical_solution(dnet)

    # testdnet=deepcopy(dnet)
    # compute_exact_network_solution(net,testdnet,ω)

    # diffnet=get_difference_of_solutions(dnet,testdnet)


    _,_,lines=SparseArrays.findnz(dnet.DiscretizedLines)

    
    return test_boundary_conditions(dnet)
    # return vector_of_line_solutions(diffnet), dnet.Δt, lines, vector_of_line_solutions(testdnet)
     # return vector_of_line_solutions(dnet)

end


